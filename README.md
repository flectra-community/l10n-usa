# Flectra Community / l10n-usa

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_us_partner_legal_number](l10n_us_partner_legal_number/) | 2.0.1.0.0| Add Legal Number for North American Banking & Financials
[account_banking_ach_credit_transfer](account_banking_ach_credit_transfer/) | 2.0.1.0.0| Create ACH files for Credit Transfers
[l10n_us_form_1099](l10n_us_form_1099/) | 2.0.1.0.0| Manage 1099 Types and Suppliers
[account_banking_ach_direct_debit](account_banking_ach_direct_debit/) | 2.0.1.0.0| Create ACH files for Direct Debit
[account_banking_ach_base](account_banking_ach_base/) | 2.0.1.0.0| Add fields required for North American Banking & Financials


